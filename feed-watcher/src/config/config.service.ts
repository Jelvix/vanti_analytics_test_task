import { IWatcherConfig } from "./config.interfaces";

import fsp from 'fs/promises'
import path from 'path'

let config: IWatcherConfig; 

async function load() {
    const configPath = path.resolve(
        __dirname,
        '..',
        '..',
        'config.json'
    );
    try {
        const fileData = await fsp.readFile(configPath);
 
        config = JSON.parse(fileData.toString('utf-8'));
    } catch(err) {
        console.error(`[Config] Loading failed: `, err);
        throw err;
    }
}

function get() : IWatcherConfig {
    return config;
}

export default {
    load,
    get
}