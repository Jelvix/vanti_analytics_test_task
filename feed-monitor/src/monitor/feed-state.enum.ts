export enum FeedState {
    Unknown = 0,
    Valid = 1,
    Invalid = 2
}