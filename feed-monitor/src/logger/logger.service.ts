function info(message: string) {
    console.log(`<${new Date().toISOString()}> ${message}`);
}

function error(message: string) {
    console.error(`[!] <${new Date().toISOString()}> ${message}`);
}

export default {
    info,
    error
}