# Vanti Feeds Monitor test task

## Getting Started

This repo consists of two separate programs: `feed-watcher` and `feed-monitor`.

You should run them in parallel to achieve desired effect.

Before running, install all needed dependencies for each program:

```bash
cd feed-monitor
npm install
cd ..
cd feed-watcher
npm install
```

You may also configure `feed-watcher` by modifying `config.json` file. The file **is explicitly** commited to this repo to show an example of it.

After running, you can now create or update files in feed folder, and expected output should be seen:

```
echo hello > feed/1194.batch
```

Expected output from `feed-watcher`:

```
Feed Watcher started
Received valid feed from customer 1194
```

Sample output from `feed-monitor`:

```
Feed Monitor started
<2022-02-17T09:51:29.167Z> Feed is dead, no data received in the specified time window!
<2022-02-17T09:51:46.203Z> Feed is alive again
```

## Deployment

**How would you deploy this app in prod?**

Based on the fact that feed files appear in a local folder, we may assume that this will run in a single instance on single server. Now there are two possible scenarios:

1. Deploying to bare-metal (no containerization). In this case we can make a helper script which runs two programs in background (it can be either a simple bash script, or a pm2 ecosystem file). Though in this case all application parts must be deployed manually, and this will be probably need some effort from DevOps side, probably using Infra-as-Code tools like Terraform or Ansible.
2. Deploy using containers (Docker). In this case each application (`feed-monitor` and `feed-watcher`) will run in a separate container (Dockerfile should be added to each app). Then we can use such tool as `docker-compose` to bootstrap a multi-container environment with a container for watcher, monitor, and Redis. Finally, feed local folder can be connected to `feed-watcher` container as a *mounted volume*.