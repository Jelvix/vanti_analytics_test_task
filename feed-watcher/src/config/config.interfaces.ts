export interface IWatcherConfig {
    feedFolder: string;
    customerIds: number[];
    extension: string;
}