import RedisService from './redis/redis.service'

import MonitorService from './monitor/monitor.service'

async function run() {
    try {
        await RedisService.connect();
    } catch(err) {
        console.error(`Fatal: Monitor initialization failed: `, err);
        process.exit(1);
    }

    MonitorService.startMonitoring();

    console.log(`Feed Monitor started`);
}   

run();