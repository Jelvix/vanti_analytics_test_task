import ConfigService from '../config/config.service'

import path from 'path'
import { IFileValidatorResult } from './file-validator.interfaces';

function checkFileAndExtractCustomerData(filepath: string) : IFileValidatorResult | null {
    const { name, ext } = path.parse(filepath);

    if (ext !== ConfigService.get().extension) return null;

    if (isNaN(Number(name))) return null;

    const providedCustomerId = +name;
    
    const validCustomerIds = ConfigService.get().customerIds;

    if (!validCustomerIds.includes(providedCustomerId)) return null;

    return {
        customerId: providedCustomerId
    }
}

export default {
  checkFileAndExtractCustomerData,
};