import ConfigService from "./config/config.service";
import RedisService from './redis/redis.service'

import WatcherService from './watcher/watcher.service'

async function run() {
    try {
        await ConfigService.load();
        await RedisService.connect();
    } catch(err) {
        console.error(`Fatal: watcher launch failed: `, err);
        process.exit(1);
    }   

    WatcherService.startWatching();

    console.log(`Feed Watcher started`);
}

run();