import LoggerService from "../logger/logger.service";

function handleFeedBecomingActive() {
    LoggerService.info(`Feed is alive again`);
}

function handleFeedBecomingDead() {
    LoggerService.info(`Feed is dead, no data received in the specified time window!`);
}

export default {
    handleFeedBecomingActive,
    handleFeedBecomingDead
}