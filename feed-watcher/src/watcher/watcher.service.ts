import chokidar from 'chokidar'
import path from 'path'

import ConfigService from "../config/config.service"

import FileValidatorService from "./file-validator.service"
import TrackerService from './tracker.service'

async function onFileAddedOrUpdated(addedFilePath: string) {
    const validationResult = FileValidatorService.checkFileAndExtractCustomerData(addedFilePath);

    if (!validationResult) {
        console.log(`Skipped file change: ${addedFilePath}`);
        return;
    }

    await TrackerService.trackValidFeed(validationResult.customerId);
}

function startWatching() {
    const providedFeedPath = ConfigService.get().feedFolder;

    const fullWatchPath = path.isAbsolute(
        providedFeedPath
    ) ? providedFeedPath : path.resolve(
        __dirname,
        '..',
        '..',
        providedFeedPath
    );

    const fsWatcher = chokidar.watch(fullWatchPath, {
      depth: 1,
      ignoreInitial: true
    });

    fsWatcher.on('add', onFileAddedOrUpdated);
    fsWatcher.on("change", onFileAddedOrUpdated);
}

export default {
    startWatching
}