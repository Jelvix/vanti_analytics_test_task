import RedisService from "../redis/redis.service";

const VALID_FEED_EVENT = 'received';
const FEED_TIMESTAMP_KEY = 'last_feed';

async function trackValidFeed(customerId: number) {
    await RedisService.set(FEED_TIMESTAMP_KEY, `${Date.now()}`);
    await RedisService.publishEvent(VALID_FEED_EVENT);

    console.log(`Received valid feed from customer ${customerId}`)
}

export default {
    trackValidFeed
}