import RedisService from '../redis/redis.service'
import LoggerService from '../logger/logger.service';
import { FeedState } from './feed-state.enum';
import AlertService from './alert.service';

const MONITORING_INTERVAL = 30 * 1000; //30 seconds
const WATCHER_TIMESTAMP_KEY = 'last_feed';
const VALID_FEED_EVENT = "received";
const ALERT_THRESHOLD = 1 * 60 * 1000; //1 minute

let feedLastCheckedAt: number;
let currentFeedState: FeedState;


/**
 * Checks feed state and sends alert if state changed
 * 
 * If feed watcher has not been started yet, it will use monitoring launch timestamp as a starting point for
 * checking feed state
 */
async function checkFeedState() {
    const rawLastValidFeed = await RedisService.get(WATCHER_TIMESTAMP_KEY);

    if (rawLastValidFeed) {
        feedLastCheckedAt = +rawLastValidFeed;
    }

    const isFeedValid = Date.now() - feedLastCheckedAt < ALERT_THRESHOLD;

    const previousFeedState = currentFeedState;

    currentFeedState = isFeedValid ? FeedState.Valid : FeedState.Invalid;

    if (previousFeedState !== currentFeedState) {
        if (currentFeedState === FeedState.Invalid) {
            AlertService.handleFeedBecomingDead();
        } else if (previousFeedState !== FeedState.Unknown) { //if feed is already alive at launch, do not log anything
            AlertService.handleFeedBecomingActive();
        }
    }
    
    feedLastCheckedAt = Date.now();
} 


/**
 * Starts monitoring of the feed.
 * 
 * An interval timer with MONITORING_INTERVAL will run and initiate a feed state check every iteration
 * This methods also immediately checks feed state
 */
function startMonitoring() {
    setInterval(checkFeedState, MONITORING_INTERVAL);

    setImmediate(checkFeedState);

    RedisService.getSubscriber().on('message', (channel, message) => {
        if (channel == 'feed-watcher' && message == VALID_FEED_EVENT) checkFeedState();
    });

    feedLastCheckedAt = Date.now();

    currentFeedState = FeedState.Unknown;
}

export default {
    startMonitoring,
    checkFeedState
}