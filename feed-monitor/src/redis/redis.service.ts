import ioredis from 'ioredis'

let redisClient: ioredis.Redis;
let redisSubscriber: ioredis.Redis;

async function connect() {
    redisClient = new ioredis({
        lazyConnect: true
    });

    redisSubscriber = new ioredis({
        lazyConnect: true
    });

    try {
        await redisClient.connect();
        await redisSubscriber.connect();
    } catch(err) {
        console.error(`[Redis] Connection failed:`, err);
        throw err;
    }

    await redisSubscriber.subscribe('feed-watcher');
}

async function get(key: string) {
   return await redisClient.get(`feed_watcher:${key}`);
}

function getSubscriber() {
    return redisSubscriber;
}

export default {
  connect,
  get,
  getSubscriber
};