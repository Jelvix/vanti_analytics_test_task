import ioredis from 'ioredis'

let redisClient: ioredis.Redis;

async function connect() {
    redisClient = new ioredis({
        lazyConnect: true
    });

    try {
        await redisClient.connect();
    } catch(err) {
        console.error(`[Redis] Connection failed:`, err);
        throw err;
    }
}

async function set(key: string, value: string) {
    await redisClient.set(`feed_watcher:${key}`, value);
}

async function publishEvent(message: string) {
    await redisClient.publish(`feed-watcher`, message);
}

export default {
  connect,
  set,
  publishEvent,
};